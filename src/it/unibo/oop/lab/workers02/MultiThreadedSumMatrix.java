package it.unibo.oop.lab.workers02;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MultiThreadedSumMatrix implements SumMatrix {
	
	private final int nthreads;
	
	public MultiThreadedSumMatrix(final int nthreads) {
		this.nthreads = nthreads;
	}
	
	 private static class Worker extends Thread {
	        private final double[][] matrix;
	        private final int index1;
	        private final int index2;
	        private final int nelem;
	        private long res;
	        
	        Worker(final double[][] matrix, final int index1, final int index2, final int nelem){
	        	super();
	        	this.matrix = matrix;
	        	this.index1 = index1;
	        	this.index2 = index2;
	        	this.nelem = nelem;
	        	}
	        public void run() {
	            System.out.println("Working from position (" + index1 + "; " + index2 + ") to position ("
	            					+ (index1 + nelem - 1) + "; " + (index2 + nelem - 1) + " )");
	            for (int i = index1; i < this.matrix.length && i < index1 + nelem; i++) {
	            	for (int j = index2; j < this.matrix.length && j < index2 + nelem; j++) {
	            		this.res += this.matrix[i][j];
	            	}
	                
	            }
	        }

	        public long getResult() {
	            return this.res;
	        }
	 }

	@Override
	public double sum(double[][] matrix) {
		final int size = matrix.length % nthreads + matrix.length / nthreads;
        
        final List<Worker> workers = IntStream.iterate(0, start -> start + size)
                .limit(nthreads)
                .mapToObj(start -> new Worker(matrix, start, start, size))
                .collect(Collectors.toList());
        /*
         * Start them
         */
        workers.forEach(Thread::start);
        /*
         * Wait for every one of them to finish
         */
        workers.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        });
        /*
         * Return the sum
         */
        return workers.stream().mapToLong(Worker::getResult).sum();
	}

}
