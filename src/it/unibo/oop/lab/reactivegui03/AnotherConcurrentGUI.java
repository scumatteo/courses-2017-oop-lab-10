package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AnotherConcurrentGUI extends JFrame{

	private static final long serialVersionUID = 1L;
	private static final double WIDTH_PERC = 0.2;
	private static final double HEIGHT_PERC = 0.1;
	private final JLabel display = new JLabel();
	private final JButton up = new JButton("up");
	private final JButton down = new JButton("down");
	private final JButton stop = new JButton("stop");

	public AnotherConcurrentGUI(){
		super();
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize((int)(screenSize.getWidth() * WIDTH_PERC), (int)(screenSize.getHeight() * HEIGHT_PERC));
		final JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		panel.add(display);
		panel.add(up);
		panel.add(down);
		panel.add(stop);
		this.getContentPane().add(panel);
		final AgentCounting agentcount = new AgentCounting();
		final AgentEnding agentend = new AgentEnding(agentcount);
		new Thread(agentcount).start();
		new Thread(agentend).start();
		stop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				agentcount.stopCounting();
				stop.setEnabled(false);
				up.setEnabled(false);
				down.setEnabled(false);
			}
		});
		
		up.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				agentcount.upCounting();
				
			}
			
		});
		
		down.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				agentcount.downCounting();
				
			}
		});
	}
	
	private class AgentCounting implements Runnable {
		 private volatile boolean stop;
		 private volatile boolean up = true;
	     private int counter;

		@Override
		public void run() {
			while(!this.stop) {
				try {
					SwingUtilities.invokeAndWait(new Runnable() {
						public void run() {
							AnotherConcurrentGUI.this.display.setText(Integer.toString(AgentCounting.this.counter));
						}
					});
					if(up) {
						this.counter++;
					}
					else {
						this.counter--;
					}
					Thread.sleep(100);
					
				} catch(InvocationTargetException | InterruptedException ex) {
                    ex.printStackTrace();
                }
			}
			
		}
		
		public void stopCounting() {
            this.stop = true;
        }
		
		public void upCounting() {
            this.up = true;
        }
		
		public void downCounting() {
            this.up = false;
        }
		
	}
	
	private class AgentEnding extends AgentCounting implements Runnable {
		private final AgentCounting agentcount;
		private static final int SEC = 10000;
		AgentEnding(AgentCounting agent){
			this.agentcount = agent;
			
		}

		@Override
		public void run() {
				try {
					Thread.sleep(SEC);
					this.agentcount.stopCounting();
					SwingUtilities.invokeAndWait(new Runnable() {
						public void run() {
							stop.setEnabled(false);
							up.setEnabled(false);
							down.setEnabled(false);
						}
					});
					
					
				} catch(Exception e) {
					e.printStackTrace();
			}
			
		}
		
	}
	

}
